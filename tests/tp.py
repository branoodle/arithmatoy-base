# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if n == 0:
        return "0"
    else:
        return S(nombre_entier(n-1))

def S(n: str) -> str:
    return "S" + n 

    
def addition(a: str, b: str) -> str:
    if a == "0":
        return b 
    # sinon, si a.startswitch("S") on traduit en python la deuxieme règle
    elif a.startswith('S'):
        return S(addition(a[1:],b))

def multiplication(a: str, b: str) -> str:
    if a == "0":
        return "0"
    elif a[0] == 'S':
        return addition(multiplication(a[1:],b),b)


def facto_ite(n: int) -> int:
    res = 1
    for i in range(1, n +1):
        res*=i

    return res

def facto_rec(n: int) -> int:
    if n == 0:
        return 1

    return n * facto_rec(n - 1)

def fibo_rec(n: int) -> int:
    if n <= 1:
        return n 
    else:
        return ((fibo_rec(n - 1)) + (fibo_rec(n - 2)))

def fibo_ite(n: int) -> int:
    if n <= 1:
        return n
    
    a,b = 0,1
    for i in range(n):
        a,b = b,a+b
    return a



def golden_phi(n: int) -> int:
    return fibo_ite(n+1)/fibo_ite(n)    

def sqrt5(n: int) -> int:
    # phi = (1 + sqrt5) / 2
    # 2.phi = 1 + sqrt5
    # 2phi - 1= sqrt5 
    phi = golden_phi(n)
    return ((2 * phi) - 1)

def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    elif n == 1:
        return a
    elif n < 0:
        return 1 / pow(a,-n)
    elif n % 2 == 0:
        return pow(a*a, n//2)
    else:
        return a * pow(a*a, n//2)
